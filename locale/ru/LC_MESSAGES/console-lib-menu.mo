��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �      K   4  O   �  2   �  -        1  �   O  -   �  g     J   k  }   �  i   4                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Andrei Stepanov, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 <Enter> выбрать параметр, <q> выход, <h> справка <Enter> выбрать параметр, <q> возврат, <h> справка Вы УВЕРЕНЫ, что хотите %s (y/N)  Не могу записать в файл %s Файл %s не найден Нажмите «q», когда закончите. Используйте «h» для справки и прочих команд. Вернуться в главное меню Слишком много строк (%s), чтобы разом заполнить весь экран Поэтому %s пересылаются в программу »less«. Используйте <Home>, для перехода к началу и <End> для перехода к окончанию Вы можете листать %s клавишами с стрелками, <page-up>, и <page-down>.  