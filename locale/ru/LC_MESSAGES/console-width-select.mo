��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �    �  .   �  �   �  "   f  k   �  -   �  =   #  &   a  @   �  :   �  W        \     m                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Andrei Stepanov, 2022
Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 %s имеет ширину %s символов Программа не смогла определить ширину консоли. Активирована ли функция "Framebuffer"? Шрифтов не найдено Нажмите: <Enter> для выбора шрифте <q> для выхода, <h> для справки Ширина экрана %s символов Экран %s символов в ширину, шрифт %s Выбор ширины консоли Выберите ширину консоли в символах Установить шрифт %s по умолчанию Эта программа должна быть запущена с правами root столбцов ширина шрифта 