��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    0   �  ?   "     b  )   {     �  @   �       6     >   R  @   �  C   �                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2019
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <Enter> seleccionar opção, <q> sair, <h> ajuda <Enter> seleccionar opção, <q> voltar ao Principal, <h> ajuda %s EFECTIVAMENTE? (s/N)  Não é possível escrever no ficheiro %s Ficheiro não encontrado %s Premir 'q' quando pronto. Usar 'h' para ajuda e outros comandos. Voltar ao menu principal Há demasiadas linhas %s para caberem todas no écrã. Por isso as %s serão enviadas para o programa 'menor' (less). Usar <Home> para ir para o princípio e <End> para ir para o fim Pode deslocar a %s com as teclas de seta, <page-up>, e <page-down>. 