��          D      l       �      �   9   �   #   �        �    $   �  H     1   ]  $   �                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2021
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 A largura actual é de %s caracteres Premir: <Enter> para seleccionar uma fonte <q> para sair, <h> para ajuda O écran tem %s caracteres de largura, a fonte %s Seleccionar uma fonte para a consola 