��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  (   6  $   _     �  %   �  %   �  &   �  5        C     S     h     w     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2019
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Controlo do brilho da retro-iluminação Não foram encontrados controladores Premir '%s' para sair Usar <%s> e <%s> para ajustes maiores Usar <%s> e <%s> para ajustes menores Usar <%s> e <%s> para ajustar o brilho Só o administrador (root) pode executar este script. seta para baixo seta para a esquerda página abaixo página acima seta para a direita seta para cima 