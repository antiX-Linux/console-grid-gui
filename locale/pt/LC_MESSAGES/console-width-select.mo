��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     i  B   �     �  H   �  '   3  1   [     �  .   �     �  =   �     9     A                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>, 2021
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 %s tem %s caracteres de largura Largura da consola não detectada. A 'framebuffer' está activada? Não foram encontradas fontes Premir: <Enter> para seleccionar uma fonte <q> para sair, <h> para ajuda A largura do écran é de %s caracteres O écran tem %s caracteres de largura, a fonte %s Selecionar a largura da consola Seleccionar a largura da consola em caracteres A pré-definir a fonte para %s Este programa tem que ser executado como administrador (root) colunas largura da fonte 