��          \      �       �   )   �   3   �   "   '     J     b  F   t     �  �  �  ?   �  O   
     Z     u     �  \   �                                            <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
POT-Creation-Date: 2017-10-17 12:31-0600
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Moo <moose@mail.ru>, 2017
Language-Team: Lithuanian (https://www.transifex.com/anticapitalista/teams/10162/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
 <Enter> pasirinkti parametrą, <q> išeiti, <h> rodyti žinyną <Enter> pasirinkti parametrą, <q> grįžti į Pagrindinį, <h> rodyti žinyną Ar TIKRAI norite %s (t/N)  Nepavyksta rašyti į failą %s Failas nerastas %s Viską atlikę, paspauskite "q".  Naudokite "h" žinynui rodyti ir daugeliui kitų komandų. Grįžti į pagrindinį meniu 