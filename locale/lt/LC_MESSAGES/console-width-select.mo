��    
      l      �       �      �   8        >  9   M     �  #   �     �      �  
   �  �  
       G   !     i  h   �      �  1     %   =  J   c     �     
                       	                 %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Setting default font to %s This program must be run as root font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
POT-Creation-Date: 2017-10-17 12:31-0600
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Moo <moose@mail.ru>, 2017
Language-Team: Lithuanian (https://www.transifex.com/anticapitalista/teams/10162/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
 %s yra %s simbolių pločio Nepavyko gauti pulto pločio. Ar vaizdų atnaujinimo buferis įjungtas? Nerasta jokių šriftų Paspauskite: <Enter> norėdami pasirinkti šriftą, <q> norėdami išeiti, <h> norėdami rodyti žinyną Ekranas yra %s simbolių pločio Ekranas yra %s simbolių pločio, šriftas yra %s Nustatomas numatytasis šriftas į %s Ši programa privalo būti paleista pagrindinio (root) naudotojo teisėmis šrifto plotis 