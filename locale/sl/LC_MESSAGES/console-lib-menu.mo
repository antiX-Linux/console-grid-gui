��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    .   �  >   %  (   d     �     �  J   �       H   %  #   n  >   �  ?   �                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 <Enter> izbor možnosti, <q> izhod, <h> pomoč <Enter> izberite opcijo, <q> vrnitev v glavni meni, <h> pomoč Ali ste prepričani, da želite %s (y/N) Ne morem pisati v datoteko %s Nisem našel datoteke %s Pritisnite 'q', ko končate. Uporabite 'h' za pomoč in mnogo več ukazov. Vrnitev v glavni menu Preveč je %s vrstic, da bi jih lahko vse naenkrat prikazali na zaslonu. Zato bo %s poslan v 'less' program. Uporabite <Home> za skok na začetek in <End> za skok na konec Lahko pomikate %s s smernimi tipkami, <page-up> in <page-down>. 