��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     f  D   }     �  ?   �       (   0     Y  !   s     �  $   �     �     �                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2020
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 %s je %s znakov širok Nisem mogel določiti širine konzole. Ali je vklopljen framebuffer? Nisem našel pisav Pritisnite: <Enter> za izbor pisave <q> za izhod, <h> za pomoč Zaslon je širok %s znakov Zaslon je širok %s znakov, pisava je %s Določite širino konzole Izberite širino konzole v znakih Nastavi privzeto pisavo na %s Ta program mora biti zagnan korensko stolpcev širina pisave 