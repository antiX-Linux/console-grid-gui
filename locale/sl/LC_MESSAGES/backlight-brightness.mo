��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c     ;     U     m  '   �  (   �  /   �  1        9     G  	   V  	   `     j     z     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2020
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Nadzor osvetlitve zaslona Kontroler ni bil najden Pritisnite '%s' za izhod Uporabite <%s> in <%s> za večje korake Uporabite <%s> in <%s> za manjše korake Uporabite <%s> in <%s> za nastavljane svetlosti Za zagon te skripte morate imeti korenske pravice puščica-dol puščica-levo stran-dol stran-gor puščica-desno puščica-gor 