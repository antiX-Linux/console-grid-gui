��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  {  c  $   �  *        /  #   G  #   k  5   �  9   �  	   �  	   	  	     	     	   '  	   1     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Green, 2020
Language-Team: Japanese (https://www.transifex.com/anticapitalista/teams/10162/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 バックライトの明るさ調節 コントローラが見つかりません '%s' で終了します <%s> と <%s> で大きくします <%s> と <%s> で小さくします <%s> と <%s> を使って明るさを調節します このスクリプトは root で実行してください 下矢印 左矢印 次画面 前画面 右矢印 上矢印 