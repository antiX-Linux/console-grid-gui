��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �  3   +  �   _  ?   �  �   -  ?   �  _   �  )   T  e   ~  L   �  r   1     �  +   �                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2021
Language-Team: Hindi (https://www.transifex.com/anticapitalista/teams/10162/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
 %s की चौड़ाई %s अक्षर है कंसोल चौड़ाई प्राप्ति विफल। क्या फ्रेम-बफर सक्रिय हैं? कोई मुद्रलिपि नहीं मिली दर्ज करें : <Enter>मुद्रलिपि चयन <q>बंद करने, <h>सहायता हेतु स्क्रीन चौड़ाई %s अक्षर है स्क्रीन चौड़ाई %s अक्षर, मुद्रलिपि %s है कंसोल चौड़ाई चयन अक्षर संख्या द्वारा कंसोल चौड़ाई चुनें डिफ़ॉल्ट मुद्रलिपि %s सेट की गई प्रोग्राम निष्पादन हेतु रूट होना आवश्यक है पंक्तियाँ मुद्रलिपि चौड़ाई 