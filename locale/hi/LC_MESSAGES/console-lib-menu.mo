��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    W   �  �     G   �  0   �  )     �   ;  D   �  �      e   �  r     �   �                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2021
Language-Team: Hindi (https://www.transifex.com/anticapitalista/teams/10162/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
 <Enter>विकल्प चयन, <q> बंद करें, <h> सहायता <Enter>विकल्प चयन, <q> मुख्य मेन्यू पर वापस जाएँ, <h> सहायता क्या आप %s हेतु निश्चित है? (y/N) %s फ़ाइल पर राइट विफल फ़ाइल नहीं मिली %s पूर्ण होने पर 'q' दबाएँ। सहायता व अधिक कमांड हेतु 'h' उपयोग करें।  मुख्य मेन्यू पर वापस जाएँ सभी %s वाक्य स्क्रीन पर एक साथ प्रदर्शित नहीं हो सकते हैं। अतः %s को 'कमतर' प्रोग्राम पर भेजा जाएगा। आरंभ हेतु <Home> व अंत पर जाने हेतु <End> उपयोग करें %s को तीर कुंजियाँ, <page-up>, व <page-down> द्वारा स्क्रॉल करना संभव है। 