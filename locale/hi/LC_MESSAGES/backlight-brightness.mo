��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  8      <   9  9   v  Z   �  T     Z   `  �   �  &   I  )   p  )   �  '   �  )   �  %        	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2021
Language-Team: Hindi (https://www.transifex.com/anticapitalista/teams/10162/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
 बैकलाइट आभा नियंत्रण कोई नियंत्रक नहीं मिला बंद करने हेतु  '%s' दबाएँ दीर्घ वृद्धि हेतु <%s> व <%s> उपयोग करें लघु वृद्धि हेतु <%s> व <%s> उपयोग करें आभा नियंत्रण हेतु <%s> व <%s> उपयोग करें इस स्क्रिप्ट के निष्पादन हेतु आपका रुट होना आवश्यक है नीचे-तीर कुंजी बायीं-तीर कुंजी पृष्ठ-नीचे लाएं पृष्ठ-ऊपर करें  दायीं-तीर कुंजी ऊपर - तीर कुंजी 