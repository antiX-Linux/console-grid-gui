��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  I   
  &   T  %   {  j   �  h     x   u  j   �     Y     o     �     �     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2017
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Έλεγχος φωτεινότητας οπίσθιου φωτισμού Δεν βρέθηκε ελεγκτής Πατήστε '%s' για έξοδο Χρησιμοποιήστε τα πλήκτρα <%s> και <%s> για μεγαλύτερα βήματα Χρησιμοποιήστε τα πλήκτρα <%s> και <%s> για μικρότερα βήματα Χρησιμοποιήστε τα πλήκτρα %s και %s για να ρυθμίσετε τη φωτεινότητα Χρειάζεται προνόμια root για να εκτελέσει αυτή την εφαρμογή κάτω βέλος  αριστερό βέλος σελίδα-κάτω σελίδα-πάνω δεξί βέλος επάνω βέλος 