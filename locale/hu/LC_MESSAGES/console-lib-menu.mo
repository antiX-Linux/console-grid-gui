��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    5   �  C   �  &        B     ]  [   v     �  =   �  6   +  S   b  e   �                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 <Enter> opció választás, <q> kilépés, <h> súgó <Enter> opció választás, <q> visszalépés a Main-be, <h> súgó BIZTOS benne, hogy szeretné %s (y/N)  A(z) %s fájl nem írható Fájl nem található %s Nyomja meg a 'q'-t amikor végzett. Használja a 'h'-t a súgóhoz és sok más parancshoz. Visszatérés a főmenübe Túl sok %s sor van, hogy egyszerre kiférjen a képernyőre. Így hát a(z) %s a 'less' alkalmazásba lesz küldve. A <Home> billentyűvel az elejére ugorhat, az <End> billentyűvel pedig a végére Görgetheti %s eredményét a nyíl billentyűkkel, és a <page-up> ill. <page-down> segítségével. 