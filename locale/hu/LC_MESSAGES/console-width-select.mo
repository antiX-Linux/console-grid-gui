��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �       S   (     |  ^   �      �  0     $   L  0   q  .   �  5   �                                  
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 %s %s karakter széles Nem sikerült a konzol szélességnek beolvasása. Engedélyezve van a framebuffer? Nem találhatók betűtípusok Billentyűk: <Enter> a betűtípus kiválasztásához, <q> a kilépéshez, <h> a segítséghez A képernyő %s karakter széles Képernyő %s karakter széles, a betűtípus %s Válassza ki a konzol szélességét Válassza ki a konzol szélességét karakterben Alapértelmezett beűtípus átállítása: %s Ez a programot root felhasználóként kell futtatnia oszlop betű szélesség 