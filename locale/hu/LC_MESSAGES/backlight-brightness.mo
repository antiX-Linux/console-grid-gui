��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  (   �       '   )  6   Q  5   �  ?   �  <   �     ;     H  	   T     ^     f     s     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Háttérvilágítás fényerő vezérlő Vezérlő nem található Nyomja meg a '%s' gombot a kilépéshez Használja a %s és a %s gombokat nagyobb lépésekhez Használja a %s és a %s gombokat kisebb lépésekhez Használja a %s és a %s gombokat a fényerő beállításához Rendszergazda jogokra van szükség a program futtatásához lefele-nyíl balra-nyíl page-down page-up jobbra-nyíl felfele-nyíl 