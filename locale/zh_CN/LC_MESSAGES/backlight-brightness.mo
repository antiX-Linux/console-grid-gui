��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c     �               #     B     [  %   y     �     �     �  
   �     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Jaakie Zhong, 2023
Language-Team: Chinese (China) (https://www.transifex.com/anticapitalista/teams/10162/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 背光亮度控制 无控制器 按 '%s' 退出 用 <%s> 和 <%s> 粗略调整 用 <%s> 和 <%s> 微调 用 <%s> 和 <%s>调整亮度 你需要以root用户来运行脚本 下箭头键 左箭头键 Page-Down键 Page-Up键 右箭头键 上箭头键 