��          D      l       �      �   9   �   #   �        �    %   �  M     4   V  %   �                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Michele Iezzi <miki1965@gmail.com>, 2018
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 La larghezza corrente è %s caratteri Premi: <Enter> per selezionare un carattere, <q> per uscire, <h> per la guida Lo schermo è largo %s caratteri, il carattere è %s Seleziona un carattere per la console 