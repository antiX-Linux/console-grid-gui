��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  .   )     X     r  5   �  ,   �  +   �  5        M     Z     k  	   w     �  
   �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Michele Iezzi <miki1965@gmail.com>, 2018
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Controllo luminosità della retroilluminazione Nessun controller trovato Premi '%s' per uscire Usa <%s> e <%s> per valori di regolazione più grandi Usa <%s> e <%s> per una regolazione graduale Usa <%s> e <%s> per regolare la luminosità Devi essere un utente root per eseguire questo script freccia giù freccia sinistra pagina giù pagina su freccia destra freccia su 