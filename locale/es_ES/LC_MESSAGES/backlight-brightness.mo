��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c          :     Z  "   p  $   �  &   �  ,   �               *     8     G     V     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Casper, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Control del brillo de pantalla No se ha encontrado controlador Pulsa '%s' para salir Usa <%s> y <%s> para grandes pasos Usa <%s> y <%s> para pequeños pasos Usa <%s> y <%s> para ajustar el brillo Necesitas ser root para ejecutar este script flecha abajo flecha izquierda página abajo página arriba flecha derecha flecha arriba 