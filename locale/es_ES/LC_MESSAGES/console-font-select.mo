��          D      l       �      �   9   �   #   �        �    #   �  I   �  9   B      |                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Casper, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 El ancho actual es de %s caracteres Pulsa: <Enter> para seleccionar una fuente <q> para salir, <h> para ayuda La pantalla tiene %s caracteres de ancho, la fuente es %s Selecciona una fuente de consola 