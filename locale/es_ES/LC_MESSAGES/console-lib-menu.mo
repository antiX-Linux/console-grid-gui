��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    0   �  ;     )   C  %   m     �  T   �       B     1   ^  8   �  N   �                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Casper, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <Enter> seleccionar opción <q> salir, <h> ayuda <Enter> seleccionar opción, <q> volver al Menú, <h> ayuda ¿Estás seguro de que quieres %s? (s/N)  No se puede escribir en el archivo %s Archivo no encontrado %s Pulse 'q' cuando haya terminado.  Use 'h' para obtener ayuda y muchos más comandos. Volver al menú principal Hay demasiadas %s líneas para que quepan en la pantalla a la vez. Así que el %s será enviado al programa 'menor'. Use <Home> para ir al principio y <End> para ir al final Puede desplazarse por %s con las teclas de direccion, <page-up> y <page-down>. 