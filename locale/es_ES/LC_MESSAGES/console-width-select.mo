��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     F  M   c     �  I   �  (     9   A  !   {  /   �  ,   �  '   �     "     +                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Casper, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 %s es %s caracteres de ancho No se pudo obtener el ancho de la consola. ¿Está habilitado el framebuffer? No se han encontrado fuentes Pulsa: <Enter> para seleccionar una fuente <q> para salir, <h> para ayuda La pantalla tiene %s caracteres de ancho La pantalla tiene %s caracteres de ancho, la fuente es %s Seleccione el ancho de la consola Seleccione el ancho de la consola en caracteres Estableciendo la fuente predeterminada en %s Este programa debe ejecutarse como root columnas ancho de fuente 