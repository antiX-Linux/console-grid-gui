��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    ,   �  0   �     #     =     Z  A   m     �  A   �  )     @   5  7   v                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 <Enter> velger, <q> avslutter, <h> for hjelp <Enter> velger, <q> tilbake til start, <h> hjelp Vil du virkelig %s? (y/N) Kan ikke skrive til filen %s Fant ikke filen %s Trykk Q når du er ferdig. Trykk H for hjelp og flere kommandoer. Gå tilbake til hovedmeny Det er for mange %s-linjer til å vise dem på skjermen samtidig. Derfor sendes %s til «less»-programmet. Trykk Home for å gå til starten og End for å gå til slutten. Du kan skrolle %s med piltastene, Page Up og Page Down. 