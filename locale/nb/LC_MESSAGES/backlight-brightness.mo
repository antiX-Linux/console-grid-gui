��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c     
     #     9  #   V  "   z  )   �  "   �     �     �  	   �       
             	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Styr skjermens lysstyrke Fant ingen kontroller Trykk «%s» for å avslutte Bruk <%s> og <%s> for større trinn Bruk <%s> og <%s> for mindre trinn Bruk <%s> og <%s> for å styre lysstyrken Kun root kan kjøre dette skriptet pil ned pil venstre page down page up pil høyre pil opp 