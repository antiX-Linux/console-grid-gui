��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     5  A   H     �  ?   �     �  (   �           7     U  %   s     �     �                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 %s er %s tegn bred Klarte ikke hente konsollens bredde. Er framebufferen slått på? Fant ingen skrifter Trykk Enter for å velge skrift, Q for å avslutte, H for hjelp Skjermen er %s tegn bred Skjermen er %s tegn bred, skriften er %s Velg konsollens bredde Velg konsollens bredde i tegn Bruker %s som forvalgt skrift Dette programmet må kjøres som root kolonner skriftbredde 