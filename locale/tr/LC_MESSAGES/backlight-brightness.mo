��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c     	     )     A  &   ]  &   �  4   �  4   �          !     (     7     E  
   M     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: mahmut özcan <mahmutozcan@protonmail.com>, 2021
Language-Team: Turkish (https://www.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Arkaışık Parlaklık Denetimi Denetleyici bulunamadı Çıkmak için '%s'e basın Kaba ayar için <%s> ve <%s> kullanın ince ayar için <%s> ve <%s> kullanın parlaklığı ayarlamak için <%s> ve <%s> kullanın Bu betiği çalıştırmak için kök olmalısınız aşağı ok sol ok sayfa-aşağı sayfa-yukarı sağ ok yukarı ok 