��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     Z  O   z  $   �  P   �  '   @  ?   h  !   �  3   �  &   �  E   %     k     t                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 %s fait %s caractères de large Impossible d’obtenir la largeur de la console. Le framebuffer est-il activé? Aucune police de caractère trouvée Appuyez sur: <Enter> pour choisir une police <q> pour quitter, <h> pour l’aide L’écran fait %s caractères de large La largeur de l’écran fait %s caractères , la police est %s Choix de la largeur de la console Définissez la largeur de la console en caractères La police %s sera définie par défaut Ce programme doit être lancé en tant qu’administrateur « root » colonnes largeur de police 