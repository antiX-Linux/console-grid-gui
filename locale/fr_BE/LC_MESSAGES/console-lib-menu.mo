��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    1   �  C         `  %   �     �  j   �     )  V   C  L   �  I   �  O   1                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <Enter> choisir l’option, <q> quitter, <h> aide <Enter> choisir l’option, <q> revenir au menu principal, <h> aide Etes-vous sûr de vouloir%s(o/N) Ecriture sur le fichier %s impossible Fichier introuvable %s Appuyez sur « q » quand vous avez terminé. Utilisez « h » pour l’aide et obtenir plus de commandes. Revenir au menu principal Les lignes de %s sont trop nombreuses pour être affichées entièrement à l’écran C’est pourquoi les %s vont être envoyé(e)s vers le programme « less ». Utilisez <Home> pour vous rendre au début et <End> pour atteindre la fin Vous pouvez faire défiler les %s avec les flèches, <page-up>, et <page-down>. 