��          D      l       �      �   9   �   #   �        �    '   �  ^   �  5   H     ~                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2021
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Gjerësia aktuale e shenjave është %s Shtypni: <Enter> që të përzgjidhni një palë shkronja <q> për të dalë, <h> për ndihmë Ekrani është %s shenja i gjerë, shkronjat janë %s Përzgjidhni Shkronja Konsole 