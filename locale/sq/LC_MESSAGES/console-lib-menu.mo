��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    T   �  _     #   x     �     �  c   �     6  ;   P  .   �  I   �  P                              	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2022
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Tastin <Enter> që të përzgjidhni mundësinë, <q> që të dilet, <h> për ndihmë Tastin <Enter> që të përzgjidhni mundësinë, <q> që të ktheheni te Kreu, <h> për ndihmë Jeni i SIGURT se doni të %s (y/N)  S’shkruhet dot te kartela %s S’u gjet kartelë %s Kur të keni mbaruar, shtypni “q”.  Përdorni “h” për ndihmë dhe mjaft urdhra të tjerë. Kthehu te menuja kryesore Ka shumë rreshta %s për t’i nxënë njëherësh ekrani. Ndaj %s do t’i kalohet programit “less“. Përdorni tastin <Home> që të kaloni te fillimi dhe <End> për te fundi Mund të rrëshqitni nëpër %s me tastet shigjetë, <page-up>, dhe <page-down>. 