��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     3  D   Q     �  f   �  !     @   6     w  )   �  '   �  $   �                                  
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2022
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 %s është %s shenja i gjerë S’u mor dot gjerësi konsole. A është i aktivizuar framebuffr-i? S’u gjetën shkronja Shtypni tastin: <Enter> që të përzgjidhni një lloj shkronjash, <q> që të dilet, <h> për ndihmë Ekrani është %s shenja i gjerë Ekrani është %s shenja i gjerë, lloji i shkronjave është %s Përzgjidhni Gjerësi Konsole Përzgjidhni gjerësi konsole, në shenja Po caktohen %s si shkronja parazgjedhje Ky program duhet xhiruar si rrënjë shtylla gjerësi shkronjash 