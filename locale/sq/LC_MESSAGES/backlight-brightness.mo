��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c          &     <  2   W  1   �  5   �  :   �     -     >     N     ^     l     ~     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2022
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Kontroll Ndriçimi të Pasmë S’u gjet kontrollor Shtypni “%s” përdalje Përdorni <%s> dhe <%s> për hapa më të mëdhenj Përdorni <%s> dhe <%s> për hapa më të vegjël Përdorni <%s> dhe <%s> që të rregulloni ndriçimin Lypset të jeni rrënjë, që të xhironi këtë programth shigjeta poshtë shigjeta majtas tasti Page Down tasti Page Up shigjeta djathtas shigjeta sipër 