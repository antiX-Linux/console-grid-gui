��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     *  C   C     �  H   �     �  (        0  (   J  ,   s  -   �     �     �                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Vanhoorne Michael, 2022
Language-Team: Dutch (Belgium) (https://www.transifex.com/anticapitalista/teams/10162/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
 %s is %s karakters breed Kon de consolebreedte niet ophalen. Is de framebuffer ingeschakeld? Geen lettertypes gevonden Druk: <Enter> om een font te selecteren, <q> om te stoppen, <h>voor hulp Scherm is %s karakters breed Scherm is %s karakters breed, font is %s Selecteer Console Breedte Selecteer de consolebreedte in karakters Standaard lettertype aan het instellen op %s Dit programma moet als root uitgevoerd worden kolommen letterbreedte 