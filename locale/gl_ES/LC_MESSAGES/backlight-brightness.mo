��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  '        D     b  %   y  %   �  &   �  -   �          )  #   <  #   `     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Control do brillo da retro-iluminación Non se atoparon controladores Premer '%s' para saír Usar <%s> e <%s> para axustes maiores Usar <%s> e <%s> para axustes menores Usar <%s> e <%s> para axustar o brillo Só o administrador pode executar este script ir para abaixo ir para a esquerda ir para a parte inferior da páxina ir para a parte superior da páxina ir para a dereita ir para arriba 