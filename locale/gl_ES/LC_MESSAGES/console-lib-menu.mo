��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    1   �  ?   	     I  &   a     �  E   �     �  8      6   9  C   p  2   �                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 <Enter> seleccionar opción, <q> saír, <h> axuda <Enter> seleccionar opción, <q> volver ao Principal, <h> axuda %s EFECTIVAMENTE? (y/N) Non é posible escribir no ficheiro %s Ficheiro non atopado %s Premer 'q' cando esteas listo. Usar 'h' para axuda e outros comandos. Volver ao menú principal Hai demasiadas liñas %s para caberen todas na pantalla. Por iso as %s serán enviadas para o programa 'menor'. Usar <Home> para ir para o principio e <End> para ir para ao final. Podes ir %s coas teclas, <page-up>, e <page-down>. 