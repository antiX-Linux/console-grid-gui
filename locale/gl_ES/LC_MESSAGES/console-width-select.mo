��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     G  A   g     �  J   �  )     3   5      i  .   �     �  6   �                                  
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 %s ten %s caracteres de largura Largura da consola non detectada. A 'framebuffer' está activada? Non se atoparon fontes Premer: <Enter> para seleccionar unha fonte <q> para saír, <h> para axuda A largura da pantalla é de %s caracteres A pantalla ten %s caracteres de largura, a fonte %s Seleccionar a largura da consola Seleccionar a largura da consola en caracteres A predefinir a fonte para %s Este programa ten que ser executado como administrador columnas largura da fonte 