��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �    �     �  W   �       _   -  !   �  7   �     �  5     5   :  <   p     �     �                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2022
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 %s ברוחב %s תווים לא ניתן לקבל את רוחב המסוף. האם מכלא החוזי מופעל? לא נמצאו גופנים לחיצה על: <Enter> לבחירת גופן <q> לקבלת עזרה, <h> לקבלת עזרה המסך ברוחב %s תווים המסך ברוחב %s תווים, הגופן הוא %s בחירת רוחב מסוף נא לבחור את רוחב המסוף בתווים גופן ברירת המחדל מוגדר בתור %s יש להריץ את התוכנה הזאת כמשתמש על עמודות רוחב גופן 