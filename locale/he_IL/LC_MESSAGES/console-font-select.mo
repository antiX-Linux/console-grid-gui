��          D      l       �      �   9   �   #   �            ,   *  X   W  7   �  '   �                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2021
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 הרוחב הנוכחי הוא %s תווים לחיצה על: <Enter> לבחירת גופן <q> להפסקה, <h> לקבלת עזרה המסך ברוחב %s תווים, הגופן הוא %s בחירת גופן שורת פקודה 