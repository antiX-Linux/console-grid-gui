��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z    c  -   q     �  )   �  [   �  Y   :  K   �  X   �     9     G  	   W     a     i     y     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2023
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 בקרת בהירות תאורה אחורית לא נמצא בקר יש ללחוץ על "%s" כדי לצאת יש להשתמש במקש <%s> ובמקש <%s> בשביל צעדים גדולים יותר יש להשתמש במקש <%s> ובמקש <%s> בשביל צעדים קטנים יותר יש להשתמש ב־<%s> וב־<%s> כדי לכוון את הבהירות צריך הרשאות root (משתמש על)כדי להריץ את הסקריפט הזה חץ למטה חץ ימינה page-down page-up חץ שמאלה חץ למעלה 