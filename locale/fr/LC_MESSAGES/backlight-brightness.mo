��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  /        O  !   k  5   �  5   �  ;   �  .   5     d     p          �     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Contrôle de la luminosité du rétroéclairage Aucun contrôleur détecté Appuyez sur « %s » pour quitter Utilisez <%s> et <%s> pour de plus grands incréments Utilisez <%s> et <%s> pour de plus petits incréments Utilisez <%s> et <%s> pour ajuster le niveau de luminosité Vous devez être en root pour lancer ce script Flèche bas Flèche gauche Page précédente Page suivante Flèche droite Flèche haut 