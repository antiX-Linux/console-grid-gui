��          D      l       �      �   9   �   #   �        �    )   �  Q     5   T  3   �                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 La largeur actuelle est de %s caractères Appuyez sur : <Enter> pour choisir une police <q> pour quitter, <h> pour l’aide L’écran fait %s caractères de large, la police %s Choisissez une police de caractère pour la console 