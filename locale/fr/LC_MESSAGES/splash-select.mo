��          D      l       �      �      �   G   �      �   �       �     �  h   �  '   _                          Current theme FB Background Themes Press: <Enter> to use new theme, <q> to quit, <e> to edit, <h> for help Select a Background Theme Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-10-02 20:27+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Thème actuel Thèmes de fond d’écran FB Pressez : <Enter> pour utiliser le nouveau thème, <q> pour quitter, <e> pour éditer, <h> pour l’aide Choisissez un thème de fond d’écran 