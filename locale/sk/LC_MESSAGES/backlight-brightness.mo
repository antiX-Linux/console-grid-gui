��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c     P     k     �  (   �  &   �  '   �  9        V     c  	   s     }     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Vendelín Slezák <klaykap@yandex.com>, 2019
Language-Team: Slovak (https://www.transifex.com/anticapitalista/teams/10162/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 Kontrola sily podsvietenia Žiaden kontrolér nenájdený Stlačte '%s' na zavretie Použite <%s> a <%s> pre väčšie kroky Použite <%s> a <%s> pre menšie kroky Použite <%s> a <%s> na nastavenie jasu Potrebujete byť root aby ste mohli spustiť tento skript šípka-dole šípka-doľava page-down page-up šípka-doprava šípka-hore 