��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    -     8   A  %   z      �     �  \   �     7  M   R  6   �  G   �  Z                              	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: sonny nunag <sonnynunag@yahoo.com>, 2019
Language-Team: Filipino (Philippines) (https://www.transifex.com/anticapitalista/teams/10162/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 <Enter> pumili ng isa, <q> lumabas, <h> gabay <Enter> pumili ng isa <q> Bumalik sa pasimula, <h> gabay Sigurado ka bang gusto mong %s (y/N)  Di maaring masulatan ang file %s Di matagpuan ang file %s Pindutin ang 'q' kapag tapos ka na.  Gamiitin ang 'h' para sa gabay at iba pang mga commands Magbalik sa bungad na menu Masayong marami ang %s na linya upang pagkasyahin ng paminsan sa buong screen Samakatuwid ang %s ay mapapadala sa 'less' na programa Gamitin ang <Home> para pumunta sa simula at <End> para mapunta sa waka Maari mong ma-scroll ang %s sa pamamagitang ng mga  arrow keys, <page-up>, at <page-down>. 