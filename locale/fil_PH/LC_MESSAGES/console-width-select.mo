��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     �  H   �     �  L         Q  3   r     �  (   �      �  1        >     F                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: sonny nunag <sonnynunag@yahoo.com>, 2019
Language-Team: Filipino (Philippines) (https://www.transifex.com/anticapitalista/teams/10162/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 ang %s ay %s chars ang lapad Di maaring makakuha ng lapad ng console? Naka enable ba ang framebuffer? walang fonts na natagupuan Pindutin ang: <Enter> para pumili ng font <q> para umalis, <h> para sa gabay Ang screen ay %s chars and lapad Ang screen ay %s na chars ang lapad, ang font ay %s Piliin ang lapad ng console Piliin ang lapad ng console sa character Karaniwang setting ng font sa %s Ang program na ito ay dapat paandarin bilang root columna lapad ng font 