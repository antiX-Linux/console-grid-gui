��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  %   X     ~     �  2   �  3   �  3   "  8   V     �     �     �     �     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: sonny nunag <sonnynunag@yahoo.com>, 2019
Language-Team: Filipino (Philippines) (https://www.transifex.com/anticapitalista/teams/10162/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 Timpla ng liwanag ng ilaw panglikuran Walang controller na natagpuan Pindutin ang '%s' para umalis Gamitin ang <%s> at <%s> para sa mas malaking usog Gamitin ang <%s> at <%s> para sa mas maliit na usog Gamitin ang <%s> at <%s> para timplahin ang liwanag Dapat kang maging root bago patakbuhin ang script na ito palaso-pababa palaso-kaliwa pahina-pababa pahina-paakyat palaso-kanan palaso-pataas 