��          D      l       �      �   9   �   #   �        �    1   �  o   �  G   C  (   �                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2022
Language-Team: Ukrainian (https://www.transifex.com/anticapitalista/teams/10162/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Поточна ширина - %s символів Натисніть: <Enter> для вибору шрифту <q> для виходу, <h> для довідки Екран має ширину в %s символів, а шрифт %s Оберіть шрифт консолі 