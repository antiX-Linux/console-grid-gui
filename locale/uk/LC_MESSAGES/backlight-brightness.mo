��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c  @   �  (   )  +   R  P   ~  N   �  c     V   �     �     �  	                  9     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2022
Language-Team: Ukrainian (https://www.transifex.com/anticapitalista/teams/10162/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Контроль яскравості підсвічування Контролер не знайдено Натисніть '%s' для виходу Використовуйте <%s> та <%s> для більших значень Використовуйте <%s> та <%s> для менших значень Використовуйте <%s> та <%s>, щоб відрегулювати яскравість Щоб запустити цей скрипт, вам потрібні root-права стрілка вниз стрілка вліво page-down page-up стрілка вправо стрілка вгору 