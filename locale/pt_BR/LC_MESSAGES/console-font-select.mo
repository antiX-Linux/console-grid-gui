��          D      l       �      �   9   �   #   �        �    #     R   (  3   {     �                          Current width is %s characters Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide, font is %s Select a Console Font Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 A largura atual é de %s caracteres Pressione: <Enter> para selecionar uma fonte, <q> para sair e <h> para obter ajuda A tela tem %s caracteres de largura, a fonte tem %s Selecione uma Fonte do Console 