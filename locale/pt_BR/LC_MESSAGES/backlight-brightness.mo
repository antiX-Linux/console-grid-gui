��          �      �       H     I     f     z  "   �  #   �  *   �  &   �  
   &  
   1  	   <     F     N     Z  �  c     K  !   f     �  $   �  $   �  %   �  N        d     t     �     �     �     �     	                          
                                  Backlight Brightness Control No controller found Press '%s' to quit Use <%s> and <%s> for bigger steps Use <%s> and <%s> for smaller steps Use <%s> and <%s> to adjust the brightness You need to be root to run this script down-arrow left-arrow page-down page-up right-arrow up-arrow Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:56+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Controle do Brilho da Tela Nenhum controlador foi encontrado Pressione ‘%s’ para sair Use <%s> e <%s> para ajustes maiores Use <%s> e <%s> para ajustes menores Use <%s> e <%s> para ajustar o brilho Você precisa ser root (administrador) para executar esta instrução (script) seta para baixo seta para a esquerda página para baixo página para cima seta para a direita seta para cima 