��          |      �          )   !  3   K  "        �     �  F   �       =   '  4   e  <   �  F   �  �    0     O   7  '   �  (   �     �  m   �     a  =   z  E   �  <   �  F   ;                           	                         
       <Enter> select option, <q> quit, <h> help <Enter> select option, <q> return to Main, <h> help Are you SURE you want to %s (y/N)  Cannot write to file %s File not found %s Press 'q' when you are done.  Use 'h' for help and many more commands. Return to main menu There are too many %s lines to fit on the screen all at once. Therefore the %s will be sent to the 'less' program. Use <Home> to go to the beginning and <End> to go to the end You can scroll the %s with the arrow keys, <page-up>, and <page-down>. Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <Enter> selecione a opção, <q> sair, <h> ajuda <Enter> selecione a opção, <q> voltar ao menu principal, <h> para obter ajuda Você tem CERTEZA de que deseja%s (s/N) Não é possível escrever no arquivo %s Arquivo não encontrado %s Pressione a tecla ‘q’ quando terminar. Utilize a tecla ‘h’ para obter ajuda e muitos outros comandos. Voltar ao menu principal Existem %s linhas a mais e não cabem na tela de uma só vez. Portanto as %s serão enviadas para o programa ‘inferior’ (less). Use <Home> para ir para o início e <End> para ir para o fim Você pode rolar o %s com as teclas de seta, <page-up>, e <page-down>. 