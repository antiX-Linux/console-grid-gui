��          �      �       0     1  8   E     ~  9   �     �  #   �       &        ?      Z     {  
   �  �  �     v  c   �     �  R     #   k  3   �     �  ,   �  $     =   4     r     z                        
                       	           %s is %s chars wide Could not get console width. Is the framebuffer enabled? No fonts found Press: <Enter> to select a font <q> to quit, <h> for help Screen is %s chars wide Screen is %s chars wide, font is %s Select Console Width Select the console width in characters Setting default font to %s This program must be run as root columns font width Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2017-07-13 21:57+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 %s tem largura de %s caracteres Não foi possível obter a largura do console. O suavizador de quadros (framebuffer) está ativado? Não foram encontradas fontes Pressione: <Enter> para selecionar uma fonte, <q> para sair e <h> para obter ajuda A tela tem %s caracteres de largura A tela tem %s caracteres de largura, a fonte tem %s Selecione a Largura do Console Selecione a largura do console em caracteres Configurando a fonte padrão para %s Este programa tem que ser executado como administrador (root) colunas largura de fonte 