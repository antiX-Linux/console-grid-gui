# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR antiX Linux
# This file is distributed under the same license as the antiX Development package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# joyinko <joyinko@azet.sk>, 2017
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: antiX Development\n"
"Report-Msgid-Bugs-To: translation@antixlinux.org\n"
"POT-Creation-Date: 2019-05-15 16:29-0600\n"
"PO-Revision-Date: 2017-07-13 21:57+0000\n"
"Last-Translator: joyinko <joyinko@azet.sk>, 2017\n"
"Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

msgid "<Enter> select option, <q> return to Main, <h> help"
msgstr "<Enter> vybrat volbu, <q> návrat do menu, <h> nápověda"

msgid "<Enter> select option, <q> quit, <h> help"
msgstr "<Enter> vybrat volbu, <q> konec, <h> nápověda"

msgid "Return to main menu"
msgstr "Návrat do hlavního menu"

msgid "Are you SURE you want to %s (y/N) "
msgstr "Jste si JISTÍ, že si přejete %s (y/N)"

msgid "File not found %s"
msgstr "Soubor nebyl nalezen %s"

msgid "Cannot write to file %s"
msgstr "Nelze zapsat do souboru %s "

msgid "There are too many %s lines to fit on the screen all at once."
msgstr "Příliš mnoho %s řádků pro zobrazení na obrazovce najednou."

msgid "Therefore the %s will be sent to the 'less' program."
msgstr "Proto bude %s odeslán do 'menšího' programu."

msgid "You can scroll the %s with the arrow keys, <page-up>, and <page-down>."
msgstr "Můžete posouvat %s pomocí šipek, <page-up>, a <page-down>."

msgid "Use <Home> to go to the beginning and <End> to go to the end"
msgstr "Použivejte <Home> pro návrat na začátek a <End> pro přechod na konec."

msgid "Press 'q' when you are done.  Use 'h' for help and many more commands."
msgstr ""
"Až budete hotoví stlačte 'q'. Pro nápovědu a dalśí příkazy použijte 'h'."
